import java.util.*
import kotlin.system.exitProcess

var table = Array(3) { Array(3) { "-" } }
var turn = 'x'
var arrowPoint: String = "<-"
var rowToInt = 0
var colToInt = 0
var DEBUG_SELECT_WIN_CONDITION: Int = 2
var round = 1
var scoreX = 0
var scoreY = 0
var commd = 0

fun titleDisplay() {
    loop@ while (true) {
        try {
            println("WELCOME TO TIC TAC TOE")
            println("1. START")
            println("2. SELECT WIN CONDITION  $DEBUG_SELECT_WIN_CONDITION")
            println("3. EXIT")
            print("SELECT :  ")
            var select = readLine()?.toInt()
            when (select) {
                1 -> break@loop
                2 -> DEBUG_SELECT_WIN_CONDITION = if (DEBUG_SELECT_WIN_CONDITION == 2) 1 else 2
                3 -> exitProcess(1)
            }
            println()
        } catch (e: NumberFormatException) {
            continue
        }
    }
}

fun displayTable() {
    println()
    println("ROUND : $round")
    println("TURN  : [X   $arrowPoint   O]")
    println("SCORE :  $scoreX | $scoreY")
    println("  1 2 3")
    for (x in table.indices) {
        print("${x + 1} ")
        for (y in table.indices) {
            print(table[x][y] + " ")
        }
        println()
    }
}

fun inputValue() {
    val input = Scanner(System.`in`)
    while (true) {
        try {
            print("Input row and col : ")
            rowToInt = input.nextInt()
            colToInt = input.nextInt()
            if (rowToInt !in 1..3 || colToInt !in 1..3) {
                println("please input two number between 1 - 3 ")
                continue
            } else {
                break
            }
        } catch (e: NumberFormatException) {
            println("Incorrect input please enter number.")
        }
    }
}

fun changeTurn() {
    if (turn == 'x') {
        turn = 'o'
        arrowPoint = "->"
    } else {
        turn = 'x'
        arrowPoint = "<-"
    }
}

fun checkPosition() {
    if (table[rowToInt - 1][colToInt - 1] == "-") {
        table[rowToInt - 1][colToInt - 1] = turn.toString()
    } else if (table[rowToInt - 1][colToInt - 1] != "-") {
        changeTurn()
        println("This Position is already selected!!")
    }
}

fun isWin(): Boolean {
    if (DEBUG_SELECT_WIN_CONDITION == 1) {
        if (bruteForceCheckCondition1()) {
            return true
        }
    }
    if (DEBUG_SELECT_WIN_CONDITION == 2) {
        if (bruteForceCheckCondition2()) {
            return true
        }
    }
    return false
}



fun bruteForceCheckCondition2(): Boolean {
    for (i in 1..8) {
        var line: String? = null
        when (i) {
            1 -> line = (table[0][0] + table[0][1] + table[0][2]).toString()
            2 -> line = (table[1][0] + table[1][1] + table[1][2]).toString()
            3 -> line = (table[2][0] + table[2][1] + table[2][2]).toString()

            4 -> line = (table[0][0] + table[1][0] + table[2][0]).toString()
            5 -> line = (table[0][1] + table[1][1] + table[2][1]).toString()
            6 -> line = (table[0][2] + table[1][2] + table[2][2]).toString()

            7 -> line = (table[0][0] + table[1][1] + table[2][2]).toString()
            8 -> line = (table[0][2] + table[1][1] + table[2][0]).toString()
        }
        if (line == "xxx") {
            return true
        } else if (line == "ooo") {
            return true
        }
    }
    return false
}
fun correctPositionNumbers(table: Array<Array<String>>, playerTurn: Char, row1: Int, col1: Int, row2: Int, col2: Int, row3: Int, col3: Int): Boolean {
    return (table[row1][col1] == playerTurn.toString() && table[row2][col2] == playerTurn.toString() && table[row3][col3] == playerTurn.toString())
}

fun bruteForceCheckCondition1(): Boolean {
    return (correctPositionNumbers(table,turn,0,0,0,1,0,2) ||
            correctPositionNumbers(table,turn,1,0,1,1,1,2) ||
            correctPositionNumbers(table,turn,2,0,2,1,2,2) ||

            correctPositionNumbers(table,turn,0,0,1,0,2,0) ||
            correctPositionNumbers(table,turn,0,1,1,1,2,1) ||
            correctPositionNumbers(table,turn,0,2,1,2,2,2) ||

            correctPositionNumbers(table,turn,0,0,1,1,2,2) ||
            correctPositionNumbers(table,turn,0,2,1,1,2,0))
}

fun displayWinner() {
    println("$turn is a winner !!!")
    println("PLAY AGAIN ?")
    println("1. YES")
    println("2. RETURN TO TITLE")
    val input = Scanner(System.`in`)
    commd = input.nextInt()
}

fun main() {
    titleDisplay()
    while (true) {
        displayTable()
        inputValue()
        checkPosition()
        if (isWin()) {
            displayTable()
            displayWinner()
            computeScore()
            if (commd == 1) {
                clearTable()
                continue
            }
            if (commd == 2) {
                titleDisplay()
            }
        }
        changeTurn()
    }
}

fun clearTable() {
    for (i in table.indices) {
        for (j in table.indices) {
            table[i][j] = "-"
        }

    }
}

fun computeScore() {
    if (turn == 'x') {
        scoreX += 1
    } else {
        scoreY += 1
    }
    round++
}
